const tokenTypes = {
  ACCESS: 'access',
  RESETPASSWORD: 'resetPassword',
};

export default tokenTypes;
