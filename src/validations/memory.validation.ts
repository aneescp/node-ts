import Joi from 'joi';
import { objectId } from './custom.validation';

const createMemory = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
    tags: Joi.array().optional(),
    images: Joi.array().optional(),
  }),
};

const shareMemory = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().optional(),
    tags: Joi.string().optional(),
    images: Joi.string().optional(),
    memory: Joi.string().required(),
  }),
};

const addLikeToMemory = {
  body: Joi.object().keys({
    memory: Joi.string().required().custom(objectId),
  }),
};

const addCommentToMemory = {
  body: Joi.object().keys({
    text: Joi.string().required(),
    memory: Joi.string().required().custom(objectId),
  }),
};

const searchMemory = {
  body: Joi.object().keys({
    text: Joi.string().required(),
  }),
};

export default {
  createMemory,
  addLikeToMemory,
  addCommentToMemory,
  shareMemory,
  searchMemory,
};
