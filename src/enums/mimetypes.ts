export enum MimeTypes {
  JPEG = 'image/jpeg',
  JPG = 'image/jpg',
  PNG = 'image/png',
}

export const mimeTypesArray: string[] = Object.values(MimeTypes);
