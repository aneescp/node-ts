import config from '../config/config';
import AWS from '../config/awsConfig';

class Helper {
  static uploadToAws = (photo: Buffer, path: string) => {
    return new Promise(async (resolve, reject) => {
      try {
        const s3 = new AWS.S3();
        const params = {
          Bucket: config.aws.bucket,
          Key: `${path}`,
          Body: photo,
          ContentEncoding: 'base64',
        };
        s3.upload(params, (err: any, data: object) => {
          if (err) {
            reject(err);
          }
          resolve(data);
        });
      } catch (error: any) {
        console.log('Uploading to amazon error', error);
        reject(error);
      }
    });
  };

  static codeGenerator = (): number => {
    return Math.floor(1000 + Math.random() * 9000);
  };
}

export default Helper;
