export interface createMemoryDto {
  title: string;
  description: string;
  tags?: string[];
  images?: string[];
  createdBy?: string;
}

export interface shareMemoryDto {
  title: string;
  description?: string;
  tags?: string;
  images?: string;
  memory: string;
  createdBy?: string;
}

export interface addLikeToMemoryDto {
  memory: string;
  likedBy?: string;
}

export interface addCommentToMemoryDto {
  text: string;
  memory: string;
  createdBy?: string;
}

export interface searchMemoryDto {
  text: string;
}
