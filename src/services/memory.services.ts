import { getMemoryFeeds } from './../models/mongo-queries/get-memory-feeds';
import { IComment } from './../interfaces/IComment';
import ApiError from '../utils/ApiError';
import httpStatus from 'http-status';
import MemoryModel from '../models/Memory';
import CommentModel from '../models/Comment';
import LikeModel from '../models/Like';
import { IMemory } from '../interfaces/IMemory';

import { createMemoryDto, addLikeToMemoryDto, addCommentToMemoryDto, shareMemoryDto, searchMemoryDto } from './../dto/memory.dto';

class MemoryServices {
  static createMemory = async (body: createMemoryDto, _files: any, userId: string): Promise<IMemory> => {
    try {
      body.createdBy = userId;
      return await MemoryModel.create(body);
    } catch (error: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error.message);
    }
  };
  static getUserMemory = async (userId: string): Promise<IMemory[] | []> => {
    try {
      return await MemoryModel.find({ createdBy: userId });
    } catch (error: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error.message);
    }
  };
  static addLikeToMemory = async (body: addLikeToMemoryDto, userId: string): Promise<string> => {
    try {
      const like = await LikeModel.findOne({
        memory: body.memory,
        likedBy: userId,
      });

      if (like) {
        return 'Already like the memory!';
      } else {
        body.likedBy = userId;
        await LikeModel.create(body);
        return 'Memory liked successfully!';
      }
    } catch (error: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error.message);
    }
  };
  static addCommentToMemory = async (body: addCommentToMemoryDto, userId: string): Promise<IComment> => {
    try {
      body.createdBy = userId;
      return await CommentModel.create(body);
    } catch (error: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error.message);
    }
  };
  static shareMemory = async (body: shareMemoryDto, userId: any) => {
    try {
      body.createdBy = userId;
      return await MemoryModel.create(body);
    } catch (error: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error.message);
    }
  };
  static searchMemory = async (body: searchMemoryDto): Promise<IMemory[] | []> => {
    try {
      return await MemoryModel.find({ $text: { $search: body.text } }).limit(10);
    } catch (error: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error.message);
    }
  };

  static memoryFeeds = async (userId: string): Promise<IMemory[] | []> => {
    try {
      return await MemoryModel.aggregate(getMemoryFeeds(userId));
    } catch (error: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, error.message);
    }
  };
}

export default MemoryServices;
