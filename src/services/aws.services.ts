import config from 'config/config';
import { mimeTypesArray } from 'enums/mimetypes';
import httpStatus from 'http-status';
import ApiError from 'utils/ApiError';
import { IFile, IUploadFileResponse } from './../interfaces/IFile';
import { v4 as uuid } from 'uuid';
class AwsServices {
  private uploadParams = {
    Bucket: 'bucket-name',
    contentDisposition: 'inline',
    ACL: 'public-read',
    Key: '',
    Body: undefined,
    contentType: '',
  };

  static checkPreCondition = async (file: IFile, platform: string): Promise<string> => {
    const fileSizeInMb = file.size / 1000000;
    if (fileSizeInMb > config.aws.maxFileSize) {
      throw new ApiError(
        httpStatus.UNPROCESSABLE_ENTITY,
        `${file.originalname} has size ${fileSizeInMb} MB which is greater than allowed upload size i.e. ${config.aws.maxFileSize} MB!`,
      );
    }
    if (!mimeTypesArray.includes(file.mimetype)) {
      throw new ApiError(httpStatus.UNSUPPORTED_MEDIA_TYPE, `Allowed Media Types: ${mimeTypesArray}`);
    }

    return platform + '_' + Date.now().toString() + '_' + uuid() + '_' + file.originalname;
  };

  //   static async uploadSingle(file: IFile, platform: string): Promise<IUploadFileResponse> {
  // const fileName = await this.checkPreCondition(file, platform);
  // this.uploadParams.Body = file.buffer;
  // this.uploadParams.Key = fileName;
  // this.uploadParams.contentType = file.mimetype;
  // return this.s3
  //   .upload(this.uploadParams)
  //   .promise()
  //   .then(() => {
  //     return {
  //       name: file.originalname,
  //       url: decodeURIComponent(fileName),
  //       type: file.mimetype,
  //       location: '',
  //     };
  //   })
  //   .catch((error) => {
  //     this.logger.log(error);
  //     throw new InternalServerErrorException();
  //   });
  //   }
  //   static uploadMultipleFiles = async () => {};
}

export default AwsServices;
