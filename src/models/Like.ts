import { ILike } from './../interfaces/ILike';
import { Schema, model } from 'mongoose';

const LikeSchema = new Schema<ILike>(
  {
    memory: {
      type: Schema.Types.ObjectId,
      ref: 'Memory',
      required: false,
    },
    likedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: false,
    },
  },
  {
    collection: 'likes',
    timestamps: true,
  },
);

const Like = model<ILike>('Like', LikeSchema);
export default Like;
