import mongoose from 'mongoose';

export const getMemoryFeeds = (userId: string) => {
  const aggregation = [
    {
      $match: { createdBy: { $in: [new mongoose.Types.ObjectId(userId)] } },
    },
    {
      $lookup: {
        from: 'likes',
        localField: '_id',
        foreignField: 'memory',
        as: 'likes',
      },
    },
    {
      $lookup: {
        from: 'comments',
        localField: '_id',
        foreignField: 'memory',
        as: 'comments',
      },
    },
    {
      $lookup: {
        from: 'memories',
        localField: 'memory',
        foreignField: '_id',
        as: 'memory',
      },
    },
    {
      $unset: [
        'likes.__v',
        'likes.createdAt',
        'likes.updatedAt',
        'comments.__v',
        'comments.createdAt',
        'comments.updatedAt',
        'memory.__v',
        'memory.createdAt',
        'memory.updatedAt',
      ],
    },
    {
      $project: {
        createdAt: 0,
        updatedAt: 0,
        __v: 0,
      },
    },
  ];
  return aggregation;
};
