import { Schema, model } from 'mongoose';
import { IComment } from '../interfaces/IComment';

const commentSchema = new Schema<IComment>(
  {
    text: { type: String, required: true },
    memory: {
      type: Schema.Types.ObjectId,
      ref: 'memories',
      required: false,
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'users',
      required: false,
    },
  },
  {
    collection: 'comments',
    timestamps: true,
  },
);

const Comment = model<IComment>('Comment', commentSchema);
export default Comment;
