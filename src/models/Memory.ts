import { IMemory } from './../interfaces/IMemory';
import { Schema, model } from 'mongoose';

const memorySchema = new Schema<IMemory>(
  {
    title: { type: String, required: true },
    description: { type: String, required: false },
    tags: { type: [String], required: false },
    images: { type: [String], required: false },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    memory: {
      type: Schema.Types.ObjectId,
      ref: 'Memory',
      required: false,
    },
  },
  {
    collection: 'memories',
    timestamps: true,
  },
);

const Memory = model<IMemory>('Memory', memorySchema);
export default Memory;
