import express from 'express';
import validate from '../middlewares/validate';
import MemoryValidator from '../validations/memory.validation';
import { AUTHENTICATE } from '../middlewares/authenticate';
import MemoryController from '../controllers/memory.controller';

const router = express.Router();

// createMemory
router.post('/create-memory', AUTHENTICATE, validate(MemoryValidator.createMemory), MemoryController.createMemory);

// addLikeToMemory
router.post('/add-like-to-memory', AUTHENTICATE, validate(MemoryValidator.addLikeToMemory), MemoryController.addLikeToMemory);

// addCommentToMemory
router.post('/add-comment-to-memory', AUTHENTICATE, validate(MemoryValidator.addCommentToMemory), MemoryController.addCommentToMemory);

// shareMemory
router.post('/share-memory', AUTHENTICATE, validate(MemoryValidator.shareMemory), MemoryController.shareMemory);

// getAllUserMemory
router.get('/get-user-memory', AUTHENTICATE, MemoryController.getUserMemory);

// MemoryFeeds
router.get('/memory-feeds', AUTHENTICATE, MemoryController.memoryFeeds);

// search Memory
// by tags and title & description
router.post('/search-memory', AUTHENTICATE, validate(MemoryValidator.searchMemory), MemoryController.searchMemory);

export default router;

//*** PENDING ***//
// update memory
