import { Router } from 'express';
const router = Router();
import UserRoute from './user.routes';
import MemoryRoute from './memory.routes';

const defaultRoutes = [
  {
    path: '/user',
    route: UserRoute,
  },
  {
    path: '/memory',
    route: MemoryRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

export default router;
