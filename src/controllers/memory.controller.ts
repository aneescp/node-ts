import { IRequest } from './../interfaces/IRequest';
import { Response } from 'express';
import ApiError from '../utils/ApiError';
import httpStatus from 'http-status';
import MemoryServices from '../services/memory.services';
import catchAsync from '../utils/catchAsync';

class MemoryController {
  static createMemory = catchAsync(async (req: IRequest, res: Response) => {
    try {
      const memory = await MemoryServices.createMemory(req.body, req.files, req.userId || '');
      return res.status(200).send({
        memory: memory,
        status: 200,
        msg: 'success',
      });
    } catch (e: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e.message);
    }
  });
  static getUserMemory = catchAsync(async (req: IRequest, res: Response) => {
    try {
      const memories = await MemoryServices.getUserMemory(req.userId || '');
      return res.status(200).send({
        memories,
        msg: 'Success',
        status: 200,
      });
    } catch (e: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e.message);
    }
  });
  static addLikeToMemory = catchAsync(async (req: IRequest, res: Response) => {
    try {
      const data = await MemoryServices.addLikeToMemory(req.body, req.userId || '');
      return res.status(200).send({
        msg: data,
        status: 200,
      });
    } catch (e: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e.message);
    }
  });
  static addCommentToMemory = catchAsync(async (req: IRequest, res: Response) => {
    try {
      const comment = await MemoryServices.addCommentToMemory(req.body, req.userId || '');
      return res.status(200).send({
        comment,
        msg: 'Comment has been added Successfully!',
        status: 200,
      });
    } catch (e: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e.message);
    }
  });
  static shareMemory = catchAsync(async (req: IRequest, res: Response) => {
    try {
      const memory = await MemoryServices.shareMemory(req.body, req.userId || '');
      return res.status(200).send({
        memory: memory,
        status: 200,
        msg: 'success',
      });
    } catch (e: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e.message);
    }
  });

  static searchMemory = catchAsync(async (req: IRequest, res: Response) => {
    const memories = await MemoryServices.searchMemory(req.body);
    return res.status(200).send({
      memories,
      status: 200,
      msg: 'success',
    });
  });

  static memoryFeeds = catchAsync(async (req: IRequest, res: Response) => {
    try {
      const memories = await MemoryServices.memoryFeeds(req.userId || '');
      console.log('memories', memories);
      return res.status(200).send({
        memories,
        status: 200,
        msg: 'success',
      });
    } catch (e: any) {
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e.message);
    }
  });
}

export default MemoryController;
